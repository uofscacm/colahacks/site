import React from 'react'
import styled from 'styled-components'

import { StyledOutboundButton, breakpoints, colors } from '../components/Theme'
import logo from '../images/logo-white.svg'

const Splash = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;
  background: ${colors.primary};
  color: ${colors.white};
  @media (min-width: ${breakpoints.desktop}) {
    background: linear-gradient(
      -45deg,
      ${colors.accent} ${props => props.position || '20%'},
      ${colors.primary} 0
    );
  }
`

const Info = styled.div`
  max-width: 500px;
  padding: 0px 10px;
  margin: 15px;
  text-align: center;
  @media (min-width: ${breakpoints.tablet}) {
    text-align: left;
  }
  h1 {
    color: ${colors.white};
    margin-bottom: 10px;
    line-height: 1.25;
  }
  h4 {
    margin-bottom: 0;
  }
`

const StyledLogo = styled.img`
  width: 100%;
  max-width: 300px;
`

const Landing = () => (
  <Splash>
    <StyledLogo src={logo} alt="ColaHacks Official Logo" />
    <Info>
      <h1>The University of South Carolina's Premier Hackathon is Back.</h1>

      <StyledOutboundButton
        background={colors.white}
        color={colors.dark}
        href="https://forms.gle/RBNiZNaNxYn4FKwq8"
      >
        <h4>Pre-Register Today</h4>
      </StyledOutboundButton>
    </Info>
  </Splash>
)

export default Landing
