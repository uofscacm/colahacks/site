import React from 'react'

import Layout from '../components/Layout'
import Landing from '../components/Landing'
import SEO from '../components/SEO'

const IndexPage = () => (
  <Layout>
    <SEO
      title="Home"
      keywords={[
        `cola`,
        `hacks`,
        `colahacks`,
        `colahacks 2020`,
        `columbia`,
        `university of south carolina`,
        `uofsc`,
        `programming`,
        `technology`,
        `coding`,
        `computer`,
        `science`,
        `computer science`
      ]}
    />
    <Landing />
  </Layout>
)

export default IndexPage
