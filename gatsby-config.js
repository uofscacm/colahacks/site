module.exports = {
  siteMetadata: {
    title: `ColaHacks 2020`,
    description: `ColaHacks is the University of South Carolina's premier hackathon, bringing hundreds of students together for programming and fun!`,
    author: `@colahacks`,
    siteUrl: 'https://colahacks.com'
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-netlify`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-90399761-5'
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ColaHacks 2020`,
        short_name: `Cola20`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#73000a`,
        display: `browser`,
        icon: `src/images/icon.png`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`
      }
    },
    `gatsby-plugin-sitemap`
  ]
}
