# ColaHacks 2020 [![Netlify Status](https://api.netlify.com/api/v1/badges/4ebacd1f-51c3-4c3f-9dca-98d01c8638c4/deploy-status)](https://app.netlify.com/sites/gracious-ride-9ad09e/deploys)

[ColaHacks 2020](https://colahacks.com) is the University of South Carolina's premier hackathon.

## Team

| Name            | Role                       |
| --------------- | -------------------------- |
| Dalton Craven   | Executive Director         |
| Hunter Damron   | Sponsorship Director       |
| Justin Baum     | Finance Director           |
| Joshua Nelson   | Marketing Director         |
| Colter Boudinot | Hacker Experience Director |
